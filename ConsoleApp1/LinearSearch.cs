﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class LinearSearch : SearchStrategy
    {
        public override bool Search(double[] array,double number)
        {
            int n = array.Length;
            for (int i = 0; i < n - 1; i++)
                if (array[i] == number)
                    return true;
            return false;
        }
    }
}
