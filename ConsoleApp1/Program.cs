﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 5, 8, 12, 2, 4, 13, 1 };
            SortStrategy sortStrategy = new BubbleSort();
            SearchStrategy searchStrategy = new LinearSearch();
            NumberSequence numberSequence = new NumberSequence(array);
            numberSequence.SetSortStrategy(sortStrategy);
            numberSequence.SetSearchStrategy(searchStrategy);
            Console.WriteLine(numberSequence.ToString());
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());
            Console.WriteLine(numberSequence.Search(2));
        }
    }
}
